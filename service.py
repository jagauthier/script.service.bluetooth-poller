#!/usr/bin/env python

# Dependencies:
# sudo apt-get install -y python-gobject

import threading
import time
import signal
import dbus
import dbus.service
import dbus.mainloop.glib
import gobject
import anydbm


try:
    # so we can run this outside of kodi
    import xbmc
    import xbmcgui
    no_kodi=False
except ImportError:
    no_kodi=True

import os
import random

SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'
AGENT_PATH = "/blueagent5/agent"
CAPABILITY = "KeyboardDisplay"
PAIR_DB="/tmp/pair.db"

variables={}

def log(logline):
    print "BTPOLL: " + logline

    
def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)

    if no_kodi==True:
        variables[property]=value
        # this is hackish, but if we are not running kodi, then I want to save the pairing
        # code to a DB/file for access from the kodi version of this
        if property=="pairing_code":
            db=anydbm.open(PAIR_DB, "c")
            db[property]=value
            db.close
            os.chmod(PAIR_DB, 0644)
    else:
        xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    if no_kodi==True:
        if property in variables:
            value=variables[property]
        else:
            value=""
    else:
        if property=="pairing_code":
            try:
                db=anydbm.open(PAIR_DB, "r")
                value=db[property]
                db.close()
                # this is actually setting it inside the get function..
                # scandalous...
                xbmcgui.Window(10000).setProperty(property, value)
            except:
                # only get here if the command line version isn't working...
                # But this lets us test without it
                value=""
        else:
            value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value

class BlueInfo(dbus.service.Object):
    bus = None
    mainloop = None
    device = None
    deviceAlias = None
    player = None
    connected = None
    state = None
    status = None
    track = []

    def __init__(self):

        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        dbus.service.Object.__init__(self, dbus.SystemBus(), AGENT_PATH)
        
        self.bus = dbus.SystemBus()

        self.bus.add_signal_receiver(self.changeHandler,
                bus_name="org.bluez",
                dbus_interface="org.freedesktop.DBus.Properties",
                signal_name="PropertiesChanged",
                path_keyword="path")

       
        # We're only going to register as a pairing agent outside of kodo.
        # Because in kodi:
        # Agent /blueagent5/agent replied with an error: 
        #    org.freedesktop.DBus.Python.RuntimeError, 
        #     Traceback (most recent call last):#012 
        #     File "/usr/lib/python2.7/dist-packages/dbus/service.py", 
        #     line 654, in _message_cb#012 
        #     File "/usr/lib/python2.7/dist-packages/dbus/service.py", 
        #     line 203, in _method_lookup#012RuntimeError: 
        #        function attributes not accessible in restricted mode
        #
        #  If I ever solve this it can be all rolled back into the kodi service.
        
        if no_kodi==True:
            self.pin_code=str(random.randint(1000,9999))
            set_kodi_prop("pairing_code", str(self.pin_code))
            self.registerAsDefault()
            log("Starting agent with PIN [{}]".format(self.pin_code))
            log("Agent is waiting to pair with device")
        else:
            get_kodi_prop("pairing_code")
            
        # initial setting for home screen icon    
        set_kodi_prop("btconnected", "bluetooth.png")            
        self.getBTInfo()
        self.findPlayer()
        self.update_kodi_props()
        
        
    def start(self):
        self.mainloop = gobject.MainLoop()
        self.mainloop.run()
        
    def end(self):
        if (self.mainloop):
            log("ENDING.")
            self.mainloop.quit();
        
    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        log("Agent DisplayPinCode invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        log("Agent DisplayPasskey invoked")

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        log("Agent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return self.pin_code

    @dbus.service.method(AGENT_IFACE, in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        """Always confirm"""
        log("Agent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return

    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        """Always authorize"""
        log("Agent AuthorizeService method invoked")
        return

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        log("RequestPasskey returns 0")
        return dbus.UInt32(0)

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        """Always authorize"""
        log("Agent is authorizing device [{}]".format(self.device))
        return

    @dbus.service.method(AGENT_IFACE, in_signature="", out_signature="")
    def Cancel(self):
        log("Agent pairing request canceled from device [{}]".format(self.device))

    def trustDevice(self, path):
        bus = dbus.SystemBus()
        device_properties = dbus.Interface(bus.get_object(SERVICE_NAME, path), "org.freedesktop.DBus.Properties")
        device_properties.Set(DEVICE_IFACE, "Trusted", True)

    def registerAsDefault(self):
        defmanager = dbus.Interface(self.bus.get_object(SERVICE_NAME, "/org/bluez"), "org.bluez.AgentManager1")
        defmanager.RegisterAgent(AGENT_PATH, CAPABILITY)
        defmanager.RequestDefaultAgent(AGENT_PATH)

#    def startPairing(self):
#        adapter_path = findAdapter().object_path
#        adapter = dbus.Interface(self.bus.get_object(SERVICE_NAME, adapter_path), "org.freedesktop.DBus.Properties")
#        adapter.Set(ADAPTER_IFACE, "Discoverable", True)
            
    def getBTInfo(self):
        paired_devices=0
        set_kodi_prop("bt_avail", "0")
        set_kodi_prop("paired_devices", "0")
        set_kodi_prop("bt_power", "0")
        set_kodi_prop("bt_discoverable", "0")
        set_kodi_prop("bt_pairable", "0")        
#        set_kodi_prop("connected_device", "")
#        set_kodi_prop("connected_mac", "")        
#        set_kodi_prop("connected_path", "")

        manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = manager.GetManagedObjects()

        adapter_path = None
        device_path = None
        
        # first get BT settings. like power, discoverablility, and pairing.
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                set_kodi_prop("bt_avail", "1")
                adapter_path = path
                
                if adapter_path:
                    self.adapter=self.bus.get_object("org.bluez", path)
                    adapter_properties=self.adapter.GetAll(ADAPTER_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                    if "Powered" in adapter_properties:
                        self.powered=adapter_properties["Powered"]
                    if "Discoverable" in adapter_properties:
                        self.discoverable=adapter_properties["Discoverable"]
                    if "Pairable" in adapter_properties:
                        self.pairable=adapter_properties["Pairable"]

                    set_kodi_prop("bt_power", str(self.powered))
#                    log("Powered: " + format(self.powered))
                    set_kodi_prop("bt_discoverable", str(self.discoverable))
#                    log("Discoverable: " + format(self.discoverable))
                    set_kodi_prop("bt_pairable", str(self.pairable))
#                    log("Pairable: " + format(self.pairable))
            
            # now get paired devices, that are phones only (Class: 0x5a020c / 5898764)
            if DEVICE_IFACE in interfaces:
                device_path = path
                if device_path:
                    self.device = self.bus.get_object("org.bluez", path)
                    device_properties=self.device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                    if "Class" in device_properties:
                        if device_properties["Class"]==5898764:
                            if "Paired" in device_properties:
                                if device_properties["Paired"]==1:
                                    paired_devices+=1
                                    set_kodi_prop("paired_devices", str(paired_devices))
                                    set_kodi_prop("bt_device"+str(paired_devices),device_properties["Alias"])
                                    set_kodi_prop("bt_mac"+str(paired_devices),device_properties["Address"])
                                    set_kodi_prop("bt_devpath"+str(paired_devices),path)
                            if device_properties["Connected"]==1:
                                set_kodi_prop("connected_device", device_properties["Alias"])
                                set_kodi_prop("connected_mac", device_properties["Address"])                        
                                set_kodi_prop("connected_path", path)
                                set_kodi_prop("btconnected", "bluetooth_ct.png")                        

    def findPlayer(self):
        manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = manager.GetManagedObjects()
        
        player_path = None
        for path, interfaces in objects.iteritems():
           if PLAYER_IFACE in interfaces:
                player_path = path
                break
            
        if player_path:
            self.getPlayer(player_path)
            player_properties = self.player.GetAll(PLAYER_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
            if "Status" in player_properties:
                self.status=player_properties["Status"]
            if "Track" in player_properties:
                self.track = player_properties["Track"]
                
    def getPlayer(self, path):
        self.player = self.bus.get_object("org.bluez", path)
        device_path = self.player.Get("org.bluez.MediaPlayer1", "Device", dbus_interface="org.freedesktop.DBus.Properties")
        self.getDevice(device_path)

    def getDevice(self, path):
        self.device = self.bus.get_object("org.bluez", path)
        self.deviceAlias = self.device.Get(DEVICE_IFACE, "Alias", dbus_interface="org.freedesktop.DBus.Properties")

    def changeHandler(self, interface, changed, invalidated, path):
        iface = interface[interface.rfind(".") + 1:]

        
        log("ChangeHandler (" + iface + ")")

#        print path
#        print changed
#        print iface

        if no_kodi==True:
            return
        if iface == "Adapter1":
            self.getBTInfo()

        if iface == "Device1":
            if "Trusted" in changed:
                if changed["Trusted"]==1:
                  pass
            if "Connected" in changed:
                if changed["Connected"]==0:
                    set_kodi_prop("connected_device", "")
                    set_kodi_prop("connected_mac", "")
                    set_kodi_prop("connected_path", "")
                    set_kodi_prop("btconnected", "bluetooth.png")                    
                else:
                    set_kodi_prop("btconnected", "bluetooth_ct.png")
                    self.getBTInfo()
        elif iface == "MediaControl1":
            if "Connected" in changed:
                self.connected = changed["Connected"]
                if changed["Connected"]==1:
                    self.getBTInfo()
                    self.findPlayer()
                    try:
                        self.player.Play(dbus_interface=PLAYER_IFACE)
                    except:
                        pass
                    
        elif iface == "MediaPlayer1":
            if "Track" in changed:
                self.track = changed["Track"]
                self.update_kodi_props()
            if "Status" in changed:
                self.status = (changed["Status"])
                self.update_kodi_props()
                
    def update_kodi_props(self):
        if no_kodi==True:
            return
        if self.player:
            log("update_kodi_props")
            set_kodi_prop("player_status", self.status)
            if "Artist" in self.track:
                if self.track["Artist"]=="":
                    set_kodi_prop("player_artist", "No data")
                else:
                    set_kodi_prop("player_artist", self.track["Artist"])
                    self.artist=self.track["Artist"]
            if "Title" in self.track:
                if self.track["Title"]=="":
                    set_kodi_prop("player_title", "No data")
                else:
                    set_kodi_prop("player_title", self.track["Title"])
                    self.title=self.track["Title"]
            if "Album" in self.track:
                if self.track["Album"]=="":
                    set_kodi_prop("player_album", "No data")
                else:
                    set_kodi_prop("player_album", self.track["Album"])                    
                    self.album=self.track["Album"]
            if "Duration" in self.track:
                if self.track["Duration"]=="":
                    set_kodi_prop("player_duration", "0")
                else:
                    set_kodi_prop("player_duration", self.track["Duration"])
            if "Genre" in self.track:
                if self.track["Genre"]=="":
                    set_kodi_prop("player_genre", "No data")
                else:
                	set_kodi_prop("player_genre", self.track["Genre"])
            #### check skin settings here for toaster pop up
            if self.status != "paused" and self.status != None and self.status!="stopped":
                if "Artist" in self.track and "Title" in self.track:
                    album_str=""
                    artist_str=""
                    notify=""
                    art_dl=0                    
                    try:
                        if self.artist:
                            artist_str=self.artist.replace("/", "_")
                        album_str =self.album.replace("/", "_")
                        artist_str=artist_str.lower()
                        album_str =album_str.lower()

                    except AttributeError:
                        return

                    if get_kodi_prop("media_player_active")=="":
                        if no_kodi==False:
                            # we're going to call the music interface whenever there is a track change
                            # However, we're going to set this variable, so that it will load
                            # a different XML will which simulate a toaster pop
                            set_kodi_prop("music_toaster", "1")
                            xbmc.executebuiltin('XBMC.RunScript("script.program.musicplayer")')                            
                            
        else:
            print("Waiting for media player")


def kodi_mon():
    monitor = xbmc.Monitor()
    while True:
        if monitor.waitForAbort(1):
            BlueInfo.end(player)
            break

if __name__ == "__main__":
    gobject.threads_init()
    player = None
    log("Starting up")
    
    if no_kodi==False:
        threading.Thread(target=kodi_mon).start()

    player = BlueInfo()
    player.start()
